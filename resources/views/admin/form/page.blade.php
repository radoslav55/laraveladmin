@extends('layouts.form-tabs')

@section('content-sections')

	@foreach(languages() as $language)

	<div class="tab-pane {!! $language->default ? 'active' : null !!}" id="tab_{!! $language->id !!}">

	    {!! Form::group('text', 'name-' . $language->id, 'Заглавие',[
	    	'value' => $element ? $element->content()->lang($language->id)->name : null
	    ]) !!}
	    {!! Form::group('textEditor', 'content-' . $language->id, 'Съдържание',[
	    	'value' => $element ? $element->content()->lang($language->id)->content : null
	    ]) !!}

    </div>

    @endforeach
@endsection
  
