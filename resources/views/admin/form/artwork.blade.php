@extends('layouts.form-tabs')

@section('content-sections')

    @if($element)
        {!! Form::setElement($element) !!}
    @endif

    @foreach(languages() as $language)

    <div class="tab-pane {!! $language->default ? 'active' : null !!}" id="tab_{!! $language->id !!}">


        {!! Form::group('text', 'title-' . $language->id, 'Заглавие',[
            'value' => $element ? $element->content()->lang($language->id)->title : null
        ]) !!}

        {!! Form::group('text', 'description-' . $language->id, 'Описание',[
            'value' => $element ? $element->content()->lang($language->id)->description : null
        ]) !!}

        {!! Form::group('textEditor', 'info-' . $language->id, 'Информация',[
            'value' => $element ? $element->content()->lang($language->id)->info : null
        ]) !!}


    </div>

    @endforeach
    
@endsection
  
@section('after-slider')
    {!! Form::group('imageUpload','painting','Картина',[
        'prefix' => '/images/paintings/'
    ]) !!}
@endsection