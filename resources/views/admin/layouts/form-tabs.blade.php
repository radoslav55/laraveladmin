@section('tabs')

	@include('admin.partials.languagesNavbar')

@endsection


<div class="row">
    <div class="col-md-12">
         <div class="box box-info">
            <form class="form-horizontal" method="post" action="/admin/{!! Request::segment(2) !!}/{!! $element ? $element->id : null !!}">

            	{{ method_field($element ? 'PUT' : 'POST') }}

	            <div class="box-body">
	                {!! csrf_field() !!}

					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							@yield('tabs')
						</ul>
					    <div class="tab-content">
					    	@yield('content-sections')
					    </div>
					</div>

					<div>
						@yield('after-slider')
					</div>
	            </div>
	            <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">{!! $element ? 'Редактирай' : 'Добави' !!}</button>
                </div>
            </form>
        </div>
    </div>
</div>
