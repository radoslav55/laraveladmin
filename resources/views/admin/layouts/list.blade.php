@extends('basics.table')

@section('bodyContent')

<thead>
   <tr role="row">
      @yield('header')
      <td colspan="2"></td>
   </tr>
</thead>

<tbody>
   @yield('content')
   @if($elements->count() == 0)
      <tr><td colspan="10"><h4 align="center">Няма добавени елементи</h4></td></tr>
   @endif
</tbody>

@endsection

@section('footerContent')
    {!! $elements->render() !!}
@endsection