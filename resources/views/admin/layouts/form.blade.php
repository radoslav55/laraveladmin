<div class="row">
    <div class="col-md-12">
         <div class="box box-info">
            <form class="form-horizontal" method="post" action="{!! $url !!}">
                <div class="box-body">
                    {!! csrf_field() !!}

                    @yield('body')
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">{!! $element ? 'Редактирай' : 'Добави' !!}</button>
                </div>
            </form>
        </div>
    </div>
</div>