<div class="image-upload">
    <span class="btn btn-primary btn-file">
        Избери файл… {!! Form::file($name) !!}
    </span>
    <div class="image_preview_div">
        <img class="image-preview" height="100" src="{!! $options['prefix'] . $url !!}">
    </div>
</div>