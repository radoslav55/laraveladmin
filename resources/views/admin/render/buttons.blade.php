<div style="float:right">
	@if(!$hideCreate)
	<a class="btn btn-primary" onClick="addNewElement()"><i class="fa fa-plus"></i> Добави нов елемент</a> 
	@endif
	<a class="btn btn-default" onClick="refreshView()"><i class="fa fa-repeat"></i></a>
</div>
<div style="clear:both"></div>