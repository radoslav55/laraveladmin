<div class="form-group form-group-{!! $name !!}">
    <label for="{!! $name !!}" class="col-sm-2 control-label">{!! $label !!}</label>

    <div class="col-sm-10">

	    {!! Form::$type($name,$value ?: ($element ? $element->$name : null),[
	    	'class' =>	'form-control',
	    	'id' => $name,
	    	'placeholder' => $label . '...',
	    	'prefix' => $prefix,
	    	'rules' => $validator ? $validator->rules()[$name] : null
	    ]) !!}

        <span class="help-block"></span>
    </div>

</div>