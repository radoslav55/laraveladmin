<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
    
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
            </div>
        </div>
    

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu navigation-admin-menu">

            <!-- Optionally, you can add icons to the links -->
            @foreach($navigation as $row)
            <li class="{{ $row['url'] == Request::segment(2) ? 'active' : null }}"><a href="javascript:;" menu-link-href="/admin/{{ $row['url'] . '/list' }}"><i class="fa {!! $row['icon'] ?? 'fa-link' !!}"></i> <span>{{ $row['title'] }}</span></a></li>
            @endforeach
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
