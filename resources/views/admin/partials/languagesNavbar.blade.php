@foreach(languages() as $language)
	<li class="{!! $language->default ? 'active' : null !!}">
		<a href="#tab_{!! $language->id !!}" data-toggle="tab" aria-expanded="true">{!! strtoupper($language->code) !!}</a>
	</li>
@endforeach