<script src="/js/admin.js"></script>
<script type="text/javascript">
	@if(Request::segment(2))

        View.load('/admin/{!! Request::segment(2) !!}/list');
    @else

        View.load('/admin/pages/list', function(){

		var ctx = document.getElementById("salesChart").getContext('2d');
		var salesChart = new Chart(ctx);
    @endif
</script>