<!-- Main Footer -->
<footer class="main-footer">
    
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ date('Y') }} </strong> Created and developed by <strong>Radoslav Stefanov</strong> and <strong>Nikolay Rangelov</strong>. All rights reserved!
</footer>