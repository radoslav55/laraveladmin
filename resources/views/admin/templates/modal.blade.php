<div class="content-hover-box popup-hover-box">
	<div class="content-header">
		<h1 class="popup-title"></h1>
		<button class="close-content-hover-box btn btn-danger pull-right" style="position:absolute; top:10px; right:20px"><span class="glyphicon glyphicon-remove"></span></button>
	</div>
	<div class="content"></div>
</div>