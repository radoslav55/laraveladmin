<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Panel | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" type="text/css" href="/css/admin/app.css">

</head>
<body class="hold-transition login-page">
    <div class="background-image"></div>
    <div class="white-layer">
    	<div class="centerer">
            <div class="login-box">
                <div class="login-logo" >
                    Admin Panel
                </div>
                <!-- /.login-logo -->
                <div class="login-box-body">

                    <form method="POST">
                        {!! csrf_field() !!}
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" name="username" placeholder="Потребителско име...">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" name="password" placeholder="Парола...">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="row">
                            <!-- /.col -->
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Влез</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>

                </div>
                @if(Session::has('message'))
                    <div class="alert alert-{!! Session::get('message.type') !!}">
                        {!! Session::get('message.body') !!}
                    </div>
                @endif
                <!-- /.login-box-body -->
            </div>
        </div>

    </div>
<!-- /.login-box -->

<script type="text/javascript" src="/js/login.js"></script>
<script src="/plugins/iCheck/icheck.min.js"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>
