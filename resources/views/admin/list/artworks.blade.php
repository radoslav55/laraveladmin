@extends('layouts.list')

@section('header')
    <th>Заглавие</th>
    <th>Картина</th>
    <th>Информация</th>
    <th>Описание</th>
@endsection

@section('content')
    @foreach($elements as $element)
    <tr>
        <td>{!! $element->content()->mainLang()->title !!}</td>
        <td><img src="/images/paintings/{!! $element->painting !!}" width="200"></td>
        <td>{!! $element->content()->mainLang()->info !!}</td>
        <td>{!! $element->content()->mainLang()->description !!}</td>
        <td width="1">
            <button class="btn btn-success btn-sm edit-element-button" elementId="{!! $element->id !!}">
                <span class="glyphicon glyphicon-edit"></span> Редакция
            </button>
            <td width="1">
            <button class="btn btn-danger btn-sm delete-element-button" elementId="{!! $element->id !!}">
                <span class="glyphicon glyphicon-remove"></span>
            </button>
        </td>
        </td>
    </tr>
    @endforeach
@endsection