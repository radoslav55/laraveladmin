@extends('layouts.list')

@section('header')
    <th>Заглавие</th>
    <th>Адрес</th>
@endsection

@section('content')
    @foreach($elements as $element)
    <tr>
        <td>{!! $element->content()->mainLang()->name !!}</td>
        <td>{!! $element->url !!}</td>
        <td width="1">
            <button class="btn btn-success btn-sm edit-element-button" elementId="{!! $element->id !!}">
                <span class="glyphicon glyphicon-edit"></span> Редакция
            </button>
        </td>
    </tr>
    @endforeach
@endsection