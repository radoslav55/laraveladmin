<?php

Route::group([
	'prefix' => 'admin',
	'namespace' => 'Admin'
], function(){

	Route::group([
		'middleware' => 'VerifyGuest'
	], function(){

		Route::get('login', 'AuthController@getLogin');
		Route::post('login', 'AuthController@postLogin');

		Route::get('setPassword', 'AuthController@getPasswordForm');
		Route::post('setPassword', 'AuthController@setPassword');
	});

	Route::group([
		'middleware' => 'VerifyAdmin'
	], function(){

		Route::get('/', 'PagesController@getHomePage');

		Route::get('logout', 'AuthController@logout');

		Route::get('{page}','PagesController@index');

		Route::get('pages/list', 'PagesController@list');
		Route::resource('pages', 'PagesController');

		Route::get('artworks/list', 'ArtworksController@list');
		Route::resource('artworks', 'ArtworksController');

	});
});
