<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\User;

class AuthController extends Controller
{
    
    public function getLogin(Request $request){

    	return view('admin.auth.login');
    }

    public function postLogin(Request $request){

    	if(Auth::attempt(['username' => $request->input('username'), 'password' => $request->input('password'), 'admin' => 1], true)){
    		return redirect('/admin');
    	}

    	return redirect()->to('/admin/login')->with('message',[
    		'type' => 'danger',
    		'body' => 'Грешни данни за вход'
    	]);
    }

    public function logout(){

    	Auth::logout();
    	return redirect('/admin');
    }

    public function getPasswordForm(){
        return view('admin.auth.setPassword');
    }

    public function setPassword(Request $request){

        $username = 'root';

        User::where('username', $username)->update([
            'password' => bcrypt($request->input('password'))
        ]);

        return redirect('/admin/login');
    }
}
