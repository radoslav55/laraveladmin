<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    
    public function getHomePage(){

    	// You can set the default view to show when user loged in
    	return view('admin.home');
    }

    public function index(){
    	
    }
}
