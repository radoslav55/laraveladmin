<?php

namespace App\Services\Html;

use Request;

class ListBuilder{

    public $buffer = [];

    public function begin($options){
        
        $this->buffer['top'] = 
        '
        <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">'
                    . ($options['hideCreate'] ? null : '<a href="/admin/' . Request::segment(2) . '/create" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-plus"></span> Добави</a>') . 
                '</div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">';
    }

    public function headers($headers){

        $this->buffer['tableHead'] = '<thead><tr>';
        foreach($headers as $element){
            $this->buffer['tableHead'] .= '<th class="sorting" tabindex="0" rowspan="1" colspan="1">' . $element . '</th>';
        }
        $this->buffer['tableHead'] .= '<th colspan="2"></th>';
        $this->buffer['tableHead'] .= '</tr>';

    }

    public function col($options = []){

        $this->options = $options;
        $this->buffer['tableBody'] .= '<tr>';
    }

    public function row($value){
        $this->buffer['tableBody'] .= '<td>' . $value . '</td>';
    }

    public function buttons($id){
        $this->buffer['tableBody'] .= '
        <td width="1">
            <a href="' . ($this->options['edit'] ?? ('/admin/' . Request::segment(2) . '/edit/' . $id)) . '" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Редактирай</a>
        </td>
        <td width="1">
            <a href="/admin/' . Request::segment(2) . '/delete/' . $id . '" class="btn btn-danger btn-sm" ask-before-action><i class="fa fa-remove"></i></a>
        </td>';
    }

    public function endcol(){
        
        $this->buffer['tableBody'] .= '</tr>';
    }

    public function pages($data){

        $this->buffer['pages'] = $data;
    }
                                
    public function end(){
        echo $this->buffer['top'];
        echo $this->buffer['tableHead'];
        echo $this->buffer['tableBody'];
        echo '                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">';
        echo $this->buffer['pages']->render();
        echo '          </div>
                    </div>
                </div>
            </div>
        </div>
        </div>';
    }
}

   
        