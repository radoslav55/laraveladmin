<?php

namespace App\Services\Html;

use Input;
use Session;
use \Illuminate\Support\MessageBag;
use Request;


class FormBuilder extends \Collective\Html\FormBuilder {

    public $element = false;
    
    /**
     * group Method
     * Calls the group view for standart forms with specifically given data
     * 
     * @param  string $type - The type of the input
     * @param  string $name - The input name
     * @param  string $label - The label value
     * @param  array $options [Optional] - Special options
     * @return String [View]
     */
    public function group(string $type, string $name, string $label,array $options = []){

        return view('admin.render.form-group',[
            'type' => $type,
            'name' => $name,
            'label' => $label,
            'element' => $this->element,
            'value' => $options['value'] ?? false,
            'prefix' => $options['prefix'] ?? false
        ]);
    }
    
    /**
     * setElement method
     * Saves a given element instance to fill the inputs with it's data
     * 
     * @param Object [Model] - The needed element
     */
    public function setElement($element){   

        $this->element = $element;
    }

    /**
     * imageUpload method
     * One of the custom fields
     * Renders a box where the user can upload an image and preview it
     * 
     * @param  string $name - The name of the input
     * @param  string $url [Optional] - The url to preview a image
     * @param  array  $options [Optional] - Special options
     * @return String [View]
     */
    public function imageUpload(string $name, $url = '', $options = []){

        return view('admin.render.form-imageUpload',[
            'name' => $name,
            'url' => $url,
            'options' => $options
        ]);
    }

    public function textEditor(string $name, $content, $options = []){
        
        return view('admin.render.form-textEditor',[
            'name' => $name,
            'content' => $content,
            'options' => $options
        ]);
    }

    /* AdminLTE new helpers */

    public function prepare($data){

        $this->editing = true;
        $this->data = $data ?: false;
    }

    public function editor($name, $value, $options){
        echo '<textarea class="ckeditor" name="' . $name . '" rows="10" cols="80">' . ($value ?? $this->data->$name) . '</textarea>';
    }

    public function submit()
    {
        return parent::submit($this->editing ? 'Редактирай' : 'Добави',[
            'class' => 'btn btn-cons btn-primary'
        ]);
    }

    public function gallery($name, $gallery){
        ?> 
        <div class="box input-gallery">
            <input type="file" class="file-to-upload">
            <div class="box-header">
                <button class="btn btn-primary btn-xs btn-upload"><span class="glyphicon glyphicon-plus"></span> Добави нова снимка</button>
            </div>
            <div class="box-body no-padding">
                <table class="table">
                    <tbody> 
                        <?php foreach($gallery['data'] as $image): ?>
                       
                        <tr>
                            <td><?= $image->id ?></td>
                            <td><img src="/images/<?= Request::segment(2) . '/' . $image->image ?>" width="150"></td>
                            <td style="width:30%">
                                
                            </td>
                            <td style="text-align:center">
                                <button type="button" url="/admin/<?= Request::segment(2); ?>/markimage/<?= $image->id; ?>" class="btn <?= $image->main ? 'btn-warning' : 'btn-default' ?> btn-sm mark-image-as-main"><i class="fa fa-star"></i></button>
                                <button type="button" url="/admin/<?= Request::segment(2); ?>/removeimage/<?= $image->id; ?>" class="btn btn-danger btn-sm remove-image-from-gallery" ask-before-action><i class="fa fa-remove"></i></a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <button class="btn btn-primary btn-xs btn-upload"><span class="glyphicon glyphicon-plus"></span> Добави нова снимка</button>
            </div>
            <!-- /.box-body -->
        </div>
        <?php 
    }

    public function toggle($name, $data){

        echo '<br><input type="checkbox" name="' . $name . '" data-toggle="toggle" data-size="small" data-on="Да" data-off="Не" ' . ($this->data->$name ? 'checked' : null) . '>';
    }

}