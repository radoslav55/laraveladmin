<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request;

class NavbarProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        
        // Check if the user is currently in the admin panel
        if(Request::segment(1) == 'admin'){

            view()->share('navigation',[
                [
                    'title' => 'Начало',
                    'url' => '/',
                    'icon' => 'fa-house'
                ],
                [
                    'title' => 'Страници',
                    'url' => 'pages',
                    'icon' => 'fa-columns'
                ],
                [
                    'title' => 'Произведения',
                    'url' => 'artworks',
                    'icon' => 'fa-link'
                ]
            ]);
        
        }
    }
}
