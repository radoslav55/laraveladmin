const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

/*

elixir(function(mix) {

	// Site SASS
	mix.sass('app.scss')
		.webpack('app.js');

	// Main site css
	mix.styles([
		'main.css'
	]);

	// Styles for login page in the admin panel
	mix.styles([
		'admin/app.css',
		'admin/custom.css',
		'admin/AdminLTE.css',
		'admin/bootstrap-toggle.min.css',
		'admin/bootstrap.css',
		'admin/login/main.css',
	], 'public/css/admin/login.css');

	mix.scripts([
		'jquery.min.js',
		'admin/bootstrap.min.js',
		'admin/app.js',
	], 'public/js/login.js');


	// Styles for the main AdminLTE template
	
	mix.styles([
		'admin/app.css',
		'admin/bootstrap.css',
		'admin/AdminLTE.css',
		'admin/skins/skin-blue.css',
		'admin/custom.css'
	], 'public/css/admin/main.css');

	mix.scripts([
		'jquery.min.js',
		'admin/bootstrap.min.js',
		'admin/app.js',
		'admin/Application.js',
		'admin/modal.js',
		'admin/view.js',
		'admin/functions.js'
	], 'public/js/admin.js');


})
 */


elixir(function(mix) {

	mix.sass('admin.scss',
		'public/css/admin/app.css');

	mix.webpack('app.js',
		'public/js/admin/app.js');

	mix.sass('app.scss')
       .webpack('app.js');

});